{% from "users/map.jinja" import users with context %}

{% for username, user_data in users.items() %}
create_{{ username }}:
  user.present:
    - name: {{ username }}
    - shell: {{ user_data.shell }}
    - home: {{ user_data.home }}

{% if user_data.service_account is defined and user_data.service_account %}
generate_ssh_key_{{ username }}:
  cmd.run:
    - name: ssh-keygen -q -N '' -f {{ user_data.home }}/.ssh/id_rsa
    - runas: {{ username }}
    - unless: test -f {{ user_data.home }}/.ssh/id_rsa
{% endif %}

{% if user_data.ssh_auth_key is defined and user_data.ssh_auth_key %}
set_ssh_auth_key_{{ username }}:
  ssh_auth.present:
    - user: {{ username }}
    - source: {{ user_data.ssh_auth_key }}
{% endif %}

{% endfor %}
