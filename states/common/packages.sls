common_packages:
  pkg.latest:
    - pkgs:
      - tree
      - nano
      - tmux
      - mc
      - zip
      - unzip
      - bash-completion
      - net-tools
      - htop
    - refresh: true
