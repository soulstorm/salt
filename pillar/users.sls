users:
  alice:
    shell: /bin/bash
    home: /home/alice
    service_account: True
    groups:
      - sudo
      - alice
    ssh_auth_keys: 
      - 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDFeT6NKXfFc32CC65ppd3qEWOExbAFzgda2BPRrwx2pqqZ11qzfBmwxfQ0x7c8M4caVsciVGVpmLwGcNVDLiIYE3urNGH09ssLpFAueJSX59p0Om8Y19nM0BsEQsn7QH5JDekmwAQw2hnzAmtBCYg7Q8VqR+qN9GHulYKJ/poNEh/OD8z28i0O+TclM8t2/3Q7l9B1jirpV56V1x08egFxFfb1xtpsM+4+IUCccLt2+N3g9kybNOgVrF+u+QRIN5M1q7jL7s5S58YMdDHXHU8gOkaDsmWBklZrCZ29I/i3rTVuI74+x2+yHtvAeu+yimoOKofNfFRUGA15KtWXt7v4PB+UfN49ysNZtpTGW6PKOuSrrOKXNEizYiAx8mktNO0deEWW4AogaW4y2094Tu46oyB7UIPVH5Zf5L7Ed+pDXE+VdCkA+AIAFiZGRSwIhV+gaDEaFEDo3JxGUJhSuxsMJWs/caJHaoNQA7965de558BmTzpQsLO3xvUHDUXgYhc= soul@slimbook'
  bob:
    home: /home/bob
    shell: /bin/bash
    service_account: True
    groups:
      - bob
  charlie:
    home: /home/charlie
    service_account: False
    groups:
      - charlie